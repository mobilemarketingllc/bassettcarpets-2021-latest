var $ = jQuery;
$(document).ready(function () {
    var hash = window.location.hash.substr(1);
    $(".fl-tabs-label").each(function () {
        if ($.trim($(this).text()).toLowerCase() == hash.toLowerCase()) {
            console.log($(this));
            change_tab($(this));
            return false;
        }
    });
    $("footer .menu-item-has-children").click(function (e) {
        if ($(window).width() <= 768) {
            e.preventDefault();
            $(this).find(">ul").toggle();
        }
    });

    $('.njba-tabs').each(function(){
        var tabs = 0;
        var tabwrap = $(this);
        $(this).children('.njba-tabs-labels').children('.njba-tabs-nav').children('.njba-tabs-label').each(function(){
            if(tabs==0){ var activeClass = "njba-tab-active"; }
            tabwrap.children('.njba-tabs-panels').children('.njba-tabs-panel:eq('+tabs+')').prepend('<div class="njba-tabs-panel-label '+activeClass+'" data-index="'+tabs+'">'+$(this).html()+'</div>');
            tabs++;
        });

        tabwrap.children('.njba-tabs-panels').children('.njba-tabs-panel').children('.njba-tabs-panel-label').click(function(){
            var crrobj = jQuery(this);
            if(jQuery(this).next('.njba-tabs-panel-content').hasClass('njba-tab-active')){
                jQuery(this).addClass('njba-tab-active');
                return false;
            }
            else{
                jQuery(this).parents('.njba-tabs-panels').children('.njba-tabs-panel').children('.njba-tabs-panel-content').slideUp("slow").removeClass('njba-tab-active');
                jQuery(this).parents('.njba-tabs-panels').children('.njba-tabs-panel').children('.njba-tabs-panel-label').removeClass('njba-tab-active');
                jQuery(this).addClass('njba-tab-active').next('.njba-tabs-panel-content').slideDown("slow").addClass('njba-tab-active');
                window.setTimeout(function(){
                    $('html,body').animate({
                        scrollTop: crrobj.parents('.fl-row:eq(0)').offset().top - 40
                    }, 'slow');
                }, 300);
            }
        });
    });
});
